package com.demoapp.alcodesonboard.gsonmodels;

import java.util.List;

public class MyFriendsModel {
    public String page;
    public String per_page;
    public String total;
    public String total_pages;
    public List<UserModel> data;
}
