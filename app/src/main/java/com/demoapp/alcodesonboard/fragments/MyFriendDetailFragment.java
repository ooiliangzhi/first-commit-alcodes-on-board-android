package com.demoapp.alcodesonboard.fragments;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.LoginActivity;
import com.demoapp.alcodesonboard.activities.MyFriendDetailActivity;
import com.demoapp.alcodesonboard.activities.MyFriendProfileActivity;
import com.demoapp.alcodesonboard.adapters.MyFriendsAdapter;
import com.demoapp.alcodesonboard.database.entities.MyFriend;
import com.demoapp.alcodesonboard.utils.DatabaseHelper;
import com.demoapp.alcodesonboard.utils.SharedPreferenceHelper;
import com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel.MyFriendsViewModel;
import com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel.MyFriendsViewModelFactory;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;


public class MyFriendDetailFragment extends Fragment {
    public static final String TAG = MyFriendDetailFragment.class.getSimpleName();

    private static final String ARG_LONG_MY_FRIEND_ID = "ARG_LONG_MY_FRIEND_ID";

    @BindView(R.id.imageview_friend_image2)
    protected ImageView mImageViewFriendImage;

    @BindView(R.id.textview_first_name)
    protected TextView mTextViewFirstName;

    @BindView(R.id.textview_last_name)
    protected TextView mTextViewLastName;

    @BindView(R.id.textview_email)
    protected TextView mTextViewEmail;

    private ProgressDialog progressDialog;

    private MyFriendsViewModel mViewModel;

    private final int REQUEST_CODE_MY_FRIEND_PROFILE = 302;

    private Unbinder mUnbinder;
    private Long mMyFriendId = 0L;

    public MyFriendDetailFragment() {
    }

    public static MyFriendDetailFragment newInstance(long id) {
        Bundle args = new Bundle();
        args.putLong(ARG_LONG_MY_FRIEND_ID, id);

        MyFriendDetailFragment fragment = new MyFriendDetailFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_friend_detail, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressDialog = new ProgressDialog(getActivity());
        if (!isConnected()) {
            Toast.makeText(getActivity(), "No network", Toast.LENGTH_LONG).show();
        }

        // Check arguments pass from previous page.
        Bundle args = getArguments();

        if (args != null) {
            mMyFriendId = args.getLong(ARG_LONG_MY_FRIEND_ID, 0);
        }

        if (!progressDialog.isShowing())
            progressDialog.setMessage("Loading...");
        progressDialog.show();


            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    initView();
                }
            }, 1000);
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }

    private boolean isConnected() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();

    }

    public void showProfilePicture(MyFriendsAdapter.DataHolder data) {
        Intent intent = new Intent(getActivity(), MyFriendProfileActivity.class);
        intent.putExtra(MyFriendProfileActivity.EXTRA_LONG_MY_FRIEND_PROFILE, data.id);

        startActivityForResult(intent, REQUEST_CODE_MY_FRIEND_PROFILE);
    }

    private void initView() {

        // TODO BAD practice, should move Database operations to Repository. (Done)
        if (mMyFriendId > 0) {
            mViewModel = new ViewModelProvider(this, new MyFriendsViewModelFactory(getActivity().getApplication())).get(MyFriendsViewModel.class);
            mViewModel.getLoadSingleFriend().observe(getViewLifecycleOwner(), new Observer<MyFriend>(){

                @Override
                public void onChanged(MyFriend myFriend) {
                    if (myFriend != null) {
                        Glide.with(getActivity())
                                .load(myFriend.getImage())
                                .into(mImageViewFriendImage);
                        mTextViewFirstName.setText(myFriend.getFirstName());
                        mTextViewLastName.setText(myFriend.getLastName());
                        mTextViewEmail.setText(myFriend.getEmail());

                        mImageViewFriendImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Intent intent = new Intent(getActivity(), MyFriendProfileActivity.class);
                                Timber.e(mMyFriendId.toString());
                                intent.putExtra(MyFriendProfileActivity.EXTRA_LONG_MY_FRIEND_PROFILE, mMyFriendId);

                                startActivityForResult(intent, REQUEST_CODE_MY_FRIEND_PROFILE);
                            }
                        });

                        mTextViewEmail.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                String mailto = "mailto:"+ myFriend.getEmail() +
                                        "?cc=" + "" +
                                        "&subject=" + Uri.encode("") +
                                        "&body=" + Uri.encode("");

                                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                                emailIntent.setData(Uri.parse(mailto));

                                try {
                                    startActivity(emailIntent);
                                } catch (ActivityNotFoundException e) {
                                    Toast.makeText(getActivity(), "Unavailable.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });


                    } else {
                        // Record not found.
                        Toast.makeText(getActivity(), "Friend not found.", Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    }


                }
            });

            if (progressDialog.isShowing())
                progressDialog.dismiss();
            mViewModel.loadSingleFriend(mMyFriendId);
        }
    }
}
