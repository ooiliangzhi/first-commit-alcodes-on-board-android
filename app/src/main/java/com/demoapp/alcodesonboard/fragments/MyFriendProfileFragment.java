package com.demoapp.alcodesonboard.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.LoginActivity;
import com.demoapp.alcodesonboard.activities.MyFriendDetailActivity;
import com.demoapp.alcodesonboard.database.entities.MyFriend;
import com.demoapp.alcodesonboard.utils.DatabaseHelper;
import com.demoapp.alcodesonboard.utils.SharedPreferenceHelper;
import com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel.MyFriendsViewModel;
import com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel.MyFriendsViewModelFactory;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MyFriendProfileFragment extends Fragment {

    public static final String TAG = MyFriendDetailFragment.class.getSimpleName();

    private static final String ARG_LONG_MY_FRIEND_PROFILE = "ARG_LONG_MY_FRIEND_PROFILE";

    @BindView(R.id.imageview_friend_profile)
    protected ImageView mImageViewFriendProfile;

    private MyFriendsViewModel mViewModel;

    private Unbinder mUnbinder;
    private Long mMyFriendId = 0L;

    public MyFriendProfileFragment() {
    }

    public static MyFriendProfileFragment newInstance(long id) {
        Bundle args = new Bundle();
        args.putLong(ARG_LONG_MY_FRIEND_PROFILE, id);

        MyFriendProfileFragment fragment = new MyFriendProfileFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_friend_profile, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Check arguments pass from previous page.
        Bundle args = getArguments();

        if (args != null) {
            mMyFriendId = args.getLong(ARG_LONG_MY_FRIEND_PROFILE, 0);
        }

        initView();
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }

    private void initView() {
        // TODO BAD practice, should move Database operations to Repository. (Done)
        if (mMyFriendId > 0) {
            mViewModel = new ViewModelProvider(this, new MyFriendsViewModelFactory(getActivity().getApplication())).get(MyFriendsViewModel.class);
            mViewModel.getLoadProfilePicture().observe(getViewLifecycleOwner(), new Observer<String>(){
                @Override
                public void onChanged(String profileImage) {
                    if (profileImage != null) {
                        Glide.with(getActivity())
                                .load(profileImage)
                                .into(mImageViewFriendProfile);
                    } else {
                        // Record not found.
                        Toast.makeText(getActivity(), "Friend not found.", Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    }
                }
            });
            mViewModel.loadProfilePicture(mMyFriendId);
        }
    }
}
