package com.demoapp.alcodesonboard.repositories;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.demoapp.alcodesonboard.adapters.MyFriendsAdapter;
import com.demoapp.alcodesonboard.database.entities.MyFriend;
import com.demoapp.alcodesonboard.database.entities.MyFriendDao;
import com.demoapp.alcodesonboard.utils.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class MyFriendsRepository {

    private static MyFriendsRepository mInstance;

    private MutableLiveData<MyFriend> friend = new MutableLiveData<>();

    private MutableLiveData<String> friendProfileImage = new MutableLiveData<>();

    private MutableLiveData<List<MyFriendsAdapter.DataHolder>> mMyFriendsAdapterListLiveData = new MutableLiveData<>();

    public static MyFriendsRepository getInstance() {
        if (mInstance == null) {
            synchronized (MyFriendsRepository.class) {
                mInstance = new MyFriendsRepository();
            }
        }

        return mInstance;
    }

    private MyFriendsRepository() {
    }

    public LiveData<List<MyFriendsAdapter.DataHolder>> getMyFriendsAdapterListLiveData() {
        return mMyFriendsAdapterListLiveData;
    }

    public void loadMyFriendsAdapterList(Context context) {
        List<MyFriendsAdapter.DataHolder> dataHolders = new ArrayList<>();
        List<MyFriend> records = DatabaseHelper.getInstance(context)
                .getMyFriendDao()
                .loadAll();

        if (records != null) {
            for (MyFriend myFriend : records) {
                MyFriendsAdapter.DataHolder dataHolder = new MyFriendsAdapter.DataHolder();
                dataHolder.image = myFriend.getImage();
                dataHolder.id = myFriend.getId();
                dataHolder.firstName = myFriend.getFirstName();
                dataHolder.lastName = myFriend.getLastName();
                dataHolder.email = myFriend.getEmail();
                dataHolders.add(dataHolder);
            }
        }

        mMyFriendsAdapterListLiveData.setValue(dataHolders);
    }

    public void addFriends(Context context, Long id, String email, String firstName, String lastName, String Avatar) {
        // Create new record.
        MyFriend myFriend = new MyFriend();
        myFriend.setId(id);
        myFriend.setEmail(email);
        myFriend.setFirstName(firstName);
        myFriend.setLastName(lastName);
        myFriend.setImage(Avatar);

        // Add record to database.
        DatabaseHelper.getInstance(context)
                .getMyFriendDao()
                .insert(myFriend);

        // Done adding record, now re-load list.
        loadMyFriendsAdapterList(context);
    }

    public void deleteFriend (Context context){
        DatabaseHelper.getInstance(context)
                .getMyFriendDao()
                .deleteAll();
        loadMyFriendsAdapterList(context);
    }

    public void loadSingleFriend(Context context, Long id){
        MyFriend myFriend = DatabaseHelper.getInstance(context)
                .getMyFriendDao()
                .load(id);

        friend.setValue(myFriend);
    }

    public LiveData<MyFriend> getLoadSingleFriend(){
        return friend;
    }

    public void loadProfilePicture(Context context, Long id){
        String profileImage = DatabaseHelper.getInstance(context)
                .getMyFriendDao()
                .load(id).getImage();

        friendProfileImage.setValue(profileImage);
    }

    public LiveData<String> getLoadProfilePicture() { return friendProfileImage; }
}
