package com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel;


import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.demoapp.alcodesonboard.adapters.MyFriendsAdapter;
import com.demoapp.alcodesonboard.database.entities.MyFriend;
import com.demoapp.alcodesonboard.repositories.MyFriendsRepository;

import java.util.List;

public class MyFriendsViewModel extends AndroidViewModel {
    private MyFriendsRepository mMyFriendsRepository;

    public MyFriendsViewModel(@NonNull Application application) {
        super(application);

        mMyFriendsRepository = MyFriendsRepository.getInstance();
    }

    public LiveData<List<MyFriendsAdapter.DataHolder>> getMyFriendsAdapterListLiveData() {
        return mMyFriendsRepository.getMyFriendsAdapterListLiveData();
    }

    public void loadMyFriendsAdapterList() {
        mMyFriendsRepository.loadMyFriendsAdapterList(getApplication());
    }

    public void addFriends(Long id, String email, String firstName, String lastName, String avatar) {
        mMyFriendsRepository.addFriends(getApplication(), id, email, firstName, lastName, avatar);
    }

    public  void DeleteFriend(){
        mMyFriendsRepository.deleteFriend(getApplication());
    }

    public void loadSingleFriend(Long id) {
        mMyFriendsRepository.loadSingleFriend(getApplication(), id);
    }

    public LiveData<MyFriend> getLoadSingleFriend() {
        return mMyFriendsRepository.getLoadSingleFriend();
    }

    public void loadProfilePicture(Long id){
        mMyFriendsRepository.loadProfilePicture(getApplication(), id);
    }

    public LiveData<String> getLoadProfilePicture(){ return mMyFriendsRepository.getLoadProfilePicture(); }

}
