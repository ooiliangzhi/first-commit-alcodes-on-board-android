package com.demoapp.alcodesonboard.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.demoapp.alcodesonboard.BuildConfig;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MainActivity;
import com.demoapp.alcodesonboard.activities.MyFriendDetailActivity;
import com.demoapp.alcodesonboard.activities.MyFriendProfileActivity;
import com.demoapp.alcodesonboard.activities.MyFriendsActivity;
import com.demoapp.alcodesonboard.adapters.MyFriendsAdapter;
import com.demoapp.alcodesonboard.database.entities.MyFriend;
import com.demoapp.alcodesonboard.gsonmodels.MyFriendsModel;
import com.demoapp.alcodesonboard.gsonmodels.UserModel;
import com.demoapp.alcodesonboard.utils.NetworkHelper;
import com.demoapp.alcodesonboard.utils.SharedPreferenceHelper;
import com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel.MyFriendsViewModel;
import com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel.MyFriendsViewModelFactory;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;





public class MyFriendsFragment extends Fragment implements MyFriendsAdapter.Callbacks{

    public static final String TAG = MyFriendsFragment.class.getSimpleName();

    @BindView(R.id.friend_recyclerview)
    protected RecyclerView mRecyclerView;

    private final int REQUEST_CODE_MY_FRIEND_DETAIL = 301;

    private final int REQUEST_CODE_MY_FRIEND_PROFILE = 302;

    private Unbinder mUnbinder;
    private MyFriendsAdapter mAdapter;
    private MyFriendsViewModel mViewModel;

    @BindView(R.id.no_friend_data_view)
    protected TextView NoDataView;

    private ProgressDialog progressDialog;

    private static Integer totalPage = 0;


    public MyFriendsFragment() {
    }

    public static MyFriendsFragment newInstance() {
        return new MyFriendsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_friends, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }


    public void callUserAPI(){

        if (!progressDialog.isShowing())
            progressDialog.setMessage("Loading...");
        progressDialog.show();
        // Call user API.
        String urlCheckTotal = BuildConfig.BASE_API_URL + "users?page=1";
        StringRequest stringRequestTotalPage = new StringRequest(Request.Method.GET, urlCheckTotal, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    //set time in mili
                    Thread.sleep(1000);

                }catch (Exception e){
                    e.printStackTrace();
                }
                // Convert JSON string to Java object.
                MyFriendsModel responseModel = new GsonBuilder().create().fromJson(response, MyFriendsModel.class);
                totalPage = Integer.parseInt(responseModel.total_pages);
                String url;
                    for(int count = 1; count<=totalPage; count++) {
                        url = BuildConfig.BASE_API_URL + "users?page=" + count;
                        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {
                                // TODO bad practice, should check user is still staying in this app or not after server response. (Done)

                                // Convert JSON string to Java object.
                                MyFriendsModel responseModel = new GsonBuilder().create().fromJson(response, MyFriendsModel.class);
                                Timber.e("Testing and retrieve database table data");
                                for (UserModel userModel : responseModel.data) {
                                    Timber.e("Test");
                                    mViewModel.addFriends(userModel.id, userModel.email, userModel.first_name, userModel.last_name, userModel.avatar);
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if (progressDialog.isShowing())
                                    progressDialog.dismiss();

                            }
                        });
                        NetworkHelper.getRequestQueueInstance(getActivity()).add(stringRequest);
                    }
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });

        NetworkHelper.getRequestQueueInstance(getActivity()).add(stringRequestTotalPage);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressDialog = new ProgressDialog(getActivity());
        mViewModel = ViewModelProviders.of(this).get(MyFriendsViewModel.class);
        mViewModel.DeleteFriend();
        if (!isConnected()) {
            Toast.makeText(getActivity(), "No network", Toast.LENGTH_LONG).show();
        }else{
            callUserAPI();
        }
        initView();
        initViewModel();
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
        mViewModel = ViewModelProviders.of(this).get(MyFriendsViewModel.class);
        mViewModel.DeleteFriend();
    }

    private boolean isConnected() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE_MY_FRIEND_DETAIL && resultCode == MyFriendDetailActivity.RESULT_CONTENT_MODIFIED) {
            // Child report content is changed, re-load list.
            mViewModel.loadMyFriendsAdapterList();
        }
    }

    @Override
    public void onListItemClicked(MyFriendsAdapter.DataHolder data) {
        Intent intent = new Intent(getActivity(), MyFriendDetailActivity.class);
        intent.putExtra(MyFriendDetailActivity.EXTRA_LONG_MY_FRIEND_ID, data.id);

        startActivityForResult(intent, REQUEST_CODE_MY_FRIEND_DETAIL);
    }

    @Override
    public void showProfilePicture(MyFriendsAdapter.DataHolder data) {
        Intent intent = new Intent(getActivity(), MyFriendProfileActivity.class);
        intent.putExtra(MyFriendProfileActivity.EXTRA_LONG_MY_FRIEND_PROFILE, data.id);

        startActivityForResult(intent, REQUEST_CODE_MY_FRIEND_PROFILE);
    }


    private void initView() {
        mAdapter = new MyFriendsAdapter();
        mAdapter.setCallbacks(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initViewModel() {
//        mViewModel = ViewModelProviders.of(this).get(MyNotesViewModel.class);
        mViewModel = new ViewModelProvider(this, new MyFriendsViewModelFactory(getActivity().getApplication())).get(MyFriendsViewModel.class);
        mViewModel.getMyFriendsAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<MyFriendsAdapter.DataHolder>>() {

            @Override
            public void onChanged(List<MyFriendsAdapter.DataHolder> dataHolders) {
                mAdapter.setData(dataHolders);
                mAdapter.notifyDataSetChanged();

                // TODO check dataHolders has data or not. (Done)
                // TODO show list if have, otherwise show label: "No data" (Done)

                if(dataHolders.isEmpty() | !isConnected()){
                    NoDataView.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                }
                else{
                    mRecyclerView.setVisibility(View.VISIBLE);
                    NoDataView.setVisibility(View.GONE);
                }
            }
        });

        // Load data into adapter.
        mViewModel.loadMyFriendsAdapterList();
    }


}
