package com.demoapp.alcodesonboard.adapters;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.demoapp.alcodesonboard.R;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyFriendsAdapter extends RecyclerView.Adapter<MyFriendsAdapter.ViewHolder>{

    private List<DataHolder> mData = new ArrayList<>();
    private Callbacks mCallbacks;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_friends, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data) {
        if (data == null) {
            mData = new ArrayList<>();
        } else {
            mData = data;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder {

        public Long id;
        public  String image;
        public String firstName;
        public String lastName;
        public String email;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.linearlayout_friend_root)
        public LinearLayout root;

        @BindView(R.id.imageview_friend_image)
        public ImageView imageView;

        @BindView(R.id.textview_friend_name)
        public TextView name;

        @BindView(R.id.textview_friend_email)
        public TextView email;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindTo(DataHolder data, Callbacks callbacks) {
            //resetViews();

            if (data != null) {
                Glide.with(root)
                        .load(data.image)
                        .into(imageView);
                name.setText(data.lastName + " " + data.firstName);
                email.setText(data.email);

                if (callbacks != null) {
                    root.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callbacks.onListItemClicked(data);
                        }
                    });

                    imageView.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            callbacks.showProfilePicture(data);
                        }
                    });
                }
            }
        }

        /*public void resetViews() {
            title.setText("");
            root.setOnClickListener(null);
            deleteButton.setOnClickListener(null);
        }*/
    }

    public interface Callbacks {

        void onListItemClicked(DataHolder data);

        void showProfilePicture(DataHolder data);
    }
}
