package com.demoapp.alcodesonboard.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.fragments.MyFriendDetailFragment;
import com.demoapp.alcodesonboard.fragments.MyFriendProfileFragment;
import com.demoapp.alcodesonboard.fragments.MyFriendsFragment;

import butterknife.ButterKnife;

public class MyFriendProfileActivity extends AppCompatActivity {

    public static final String EXTRA_LONG_MY_FRIEND_PROFILE = "EXTRA_LONG_MY_FRIEND_PROFILE";

    public static final int RESULT_CONTENT_MODIFIED = 202;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_friend_profile);

        ButterKnife.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(MyFriendProfileFragment.TAG) == null) {
            // Init fragment.
            Intent extra = getIntent();
            long noteId = 0;

            if (extra != null) {
                noteId = extra.getLongExtra(EXTRA_LONG_MY_FRIEND_PROFILE, 0);
            }

            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, MyFriendProfileFragment.newInstance(noteId), MyFriendProfileFragment.TAG)
                    .commit();
        }
    }
}
